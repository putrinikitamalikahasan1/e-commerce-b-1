from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# 1
# def pengguna(request):
#     return HttpResponse("Selamat Datang Pengguna")

def pengguna(request):
    context = {
        'nama' : 'Dede'
    }
    template = loader.get_template('pengguna.html')
    return HttpResponse(template.render(context, request))